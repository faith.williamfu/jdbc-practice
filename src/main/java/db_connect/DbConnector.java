package db_connect;

import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnector {
    
    private DbConfig dbConfig;
    
    public DbConnector() {
        Yaml yaml = new Yaml();
        InputStream inputStream = this.getClass()
                .getClassLoader()
                .getResourceAsStream("application.yml");
        this.dbConfig = yaml.load(inputStream);
    }
    
    public DbConfig getDbConfig() {
        return dbConfig;
    }
    
    public Connection createConnect() throws SQLException {
        // Need to be implement
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("cannot find db driver.");
            System.out.println(e.toString());
        }

        Connection conn = null;
        try {
            conn = DriverManager.getConnection(getDbConfig().getDatabaseURL(), getDbConfig().getUsername(), getDbConfig().getPassword());
        } catch (SQLException e) {
            System.out.println("cannot connect to db.");
            System.out.println(e.toString());
        }
        return conn;
    }
}
