package db_connect;

import model.Student;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class StudentRepo {
    private DbConnector dbConnector;

    public StudentRepo() {
        dbConnector = new DbConnector();
    }
    
    
    public boolean addStudent(Student student) {
        // Need to be implemented
        try {
            PreparedStatement preparedStatement = dbConnector.createConnect().prepareStatement("INSERT INTO student (name, age, gender, phone) VALUES (?, ?, ?, ?)");
            preparedStatement.setString(1,student.getName());
            preparedStatement.setInt(2,student.getAge());
            preparedStatement.setString(3,student.getGender());
            preparedStatement.setString(4,student.getPhone());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println("fail to add new student.");
            System.out.println(e.toString());
        }
        return false;
    }
    
    public List<Student> findStudents() {
        // Need to be implemented
        List <Student> students = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = dbConnector.createConnect().prepareStatement("SELECT id, name, age, gender, phone FROM student");
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                students.add(new Student(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getString(4),rs.getString(5)));
            }
        } catch (SQLException e) {
            System.out.println("fail to find students.");
            System.out.println(e.toString());
        }
        return students;
    }
    
    public boolean updateStudent(Student student) {
        // Need to be implemented
        try {
            PreparedStatement preparedStatement = dbConnector.createConnect().prepareStatement("UPDATE student SET `name` = ?, `age` = ?, `gender` = ?, phone = ? WHERE `id` = ?");
            preparedStatement.setString(1,student.getName());
            preparedStatement.setInt(2,student.getAge());
            preparedStatement.setString(3,student.getGender());
            preparedStatement.setString(4,student.getPhone());
            preparedStatement.setInt(5,student.getId());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println("fail to update student.");
            System.out.println(e.toString());
        }
        return false;
    }
    
    public boolean deleteStudent(Student student) {
        // Need to be implemented
        try {
            PreparedStatement preparedStatement = dbConnector.createConnect().prepareStatement("DELETE FROM student WHERE `id` = ?");
            preparedStatement.setInt(1,student.getId());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.out.println("fail to delete student.");
            System.out.println(e.toString());
        }
        return false;
    }
}
