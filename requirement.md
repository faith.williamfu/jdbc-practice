# 前置知识说明

## 必须知识：
如果您需要完成该项目, 需要具备以下知识：

##### 1. [数据库连接](https://github.com/skill-courses/DbTrainingCamp_MySQL/blob/master/DbDesign/4.%E6%95%B0%E6%8D%AE%E5%BA%93%E8%BF%9E%E6%8E%A5.md)
##### 2. [Connecting to MySQL Using the JDBC DriverManager Interface](https://dev.mysql.com/doc/connector-j/5.1/en/connector-j-usagenotes-connect-drivermanager.html)
##### 3. [Using JDBC Statement Objects to Execute SQL](https://dev.mysql.com/doc/connector-j/5.1/en/connector-j-usagenotes-statements.html)
##### 4. [Java Database Connectivity with MySQL](https://www.javatpoint.com/example-to-connect-to-the-mysql-database)
##### 5. [The try-with-resources Statement](https://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html)
##### 6. [Java – Try with Resources](https://www.baeldung.com/java-try-with-resources)
